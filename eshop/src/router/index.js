import Vue from 'vue'
import Router from 'vue-router'

import homePage from '../pages/homePage.vue'
import shoppingCar from '../pages/shoppingCar.vue'
import signUp from '../pages/signUp.vue'
import Login from '../pages/login.vue'
import SearchPage from '../pages/SearchPage.vue'
import goodsInfo from '../pages/goodsInfo.vue'
import favorites from '../pages/favorites.vue'
import History from '../pages/history.vue'

Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
	{
	  path:'/',
	  redirect:'/homePage'
	},
	{
	  path:'/login',
	  name:'Login',
	  component:Login//登陆
	},
    {
      path: '/homePage',
      name: 'homePage',
      component: homePage//首页
    },
	{
	  path: '/shoppingCar',
	  name: 'shoppingCar',
	  component: shoppingCar//购物车
	},
	{
		path:'/signup',
		name:'signUp',
		component: signUp//注册
	},
	{
		path:'/goodsInfo',
		name:'goodsInfo',
		component: goodsInfo//详情页
	},
	{
		path:'/favorites',
		name:'favorites',
		component: favorites//收藏夹
	},
	{
		path:'/Searchpage',
		name:'Searchpage',
		component: SearchPage//注册
	},
	{
		path:'/history',
		name:'history',
		component: History//注册
	}
  ]
})
