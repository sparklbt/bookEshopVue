import Vue from 'vue'
import App from './App.vue'
import routers from './router/index.js'
import iView from'view-design'
import 'view-design/dist/styles/iview.css'
import axios from 'axios'
import qs from	'qs'
Vue.config.productionTip = false
Vue.use(iView)
Vue.prototype.$axios = axios
Vue.prototype.$qs = qs

new Vue({
  render: h => h(App),
  router:routers,
  components: { App },
  template: '<App/>' 
}).$mount('#app')



